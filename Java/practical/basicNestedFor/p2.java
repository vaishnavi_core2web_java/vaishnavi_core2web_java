class p2 {
    public static void main(String[] args) {
        int rowSize = 3; // Set the desired row size

        // Loop through rows
        for (int i = 0; i < rowSize; i++) {
            // Loop to print elements in each row
            for (int j = 0; j < rowSize; j++) {
                System.out.print("* "); // You can replace '*' with any character you want
            }
            System.out.println(); // Move to the next line after each row
        }
    }
}
