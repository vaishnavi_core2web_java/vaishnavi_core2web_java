import java.io.*;
class InputDemo{
	public static void main(String[] args) throws IOexception
	{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter row");
		int row = Integer.parseInt(br.readline());
		
		for(int i =1;i<=row;i++){
			for (int j=1;j<=i;j++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
