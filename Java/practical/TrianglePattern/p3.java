import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

 class InputOutputBufferedReader {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter the first number: ");
        int num1 = Integer.parseInt(reader.readLine());

        System.out.print("Enter the second number: ");
        int num2 = Integer.parseInt(reader.readLine());

        int sum = num1 + num2;

        System.out.println("The sum of " + num1 + " and " + num2 + " is: " + sum);

        reader.close();
    }
}
