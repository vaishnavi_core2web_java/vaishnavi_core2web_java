import java.util.Scanner;
class p3 {
    public static void main(String[] args) {
        // Define the size of the array
        int size = 10;
        int[] array = new int[size];

        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the elements of the array
        System.out.println("Please enter 10 elements for the array:");

        // Read the elements from the user
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        // Initialize a StringBuilder to construct the output string
        StringBuilder output = new StringBuilder();

        // Iterate through the array and append only the even elements to the StringBuilder
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                output.append(array[i]);
                output.append(" ");
            }
        }

        // Print the output
        System.out.println("Even elements: " + output.toString().trim());

        // Close the scanner
        scanner.close();
    }
}

