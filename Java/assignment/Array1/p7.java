/*7. Write a program to print the elements of the array which is divisible by 4. Take input
from the user.
Example:
Enter size: 10
14 20 18 9 11 51 16 2 8 10
Output:
20 is divisible by 4 and its index is 1
16 is divisible by 4 and its index is 6
8 is divisible by 4 and its index is 8*/
import java.util.Scanner;

 class p7 {
    public static void main(String[] args) {
        // Define the size of the array
        int size = 10;
        int[] array = new int[size];

        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the elements of the array
        System.out.println("Please enter 10 elements for the array:");

        // Read the elements from the user
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        // Iterate through the array and print elements divisible by 4 along with their indices
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 4 == 0) {
                System.out.println(array[i] + " is divisible by 4 and its index is " + i);
            }
        }

        // Close the scanner
        scanner.close();
    }
}

