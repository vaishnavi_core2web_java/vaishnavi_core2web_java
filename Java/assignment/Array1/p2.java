import java.util.Scanner;
class p2 {
    public static void main(String[] args) {
        
        int size = 10;
        int[] array = new int[size];

        
        Scanner scanner = new Scanner(System.in);

       
        System.out.println("Please enter 10 elements for the array:");

       
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
	}
        StringBuilder output = new StringBuilder();

        
        for (int i = 0; i < array.length; i++) {
            output.append(array[i]);
            if (i < array.length - 1) {
                output.append(", ");
            }
        }

        
        System.out.println(output.toString());

        
        
    }
}

