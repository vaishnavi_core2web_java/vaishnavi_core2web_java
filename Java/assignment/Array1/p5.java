/*5. Write a program where you have to print the elements from the array which are less
than 10.Take input from the user.
Example:
Enter size: 10

11 2 18 9 10 5 16 20 8 10
Output :
2 is less than 10
9 is less than 10
5 is less than 10
8 is less than 10*/
import java.util.Scanner;

 class p5 {
    public static void main(String[] args) {
        // Define the size of the array
        int size = 10;
        int[] array = new int[size];

        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the elements of the array
        System.out.println("Please enter 10 elements for the array:");

        // Read the elements from the user
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        // Iterate through the array and print elements less than 10
        System.out.println("Elements less than 10:");
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 10) {
                System.out.println(array[i] + " is less than 10");
            }
        }

        // Close the scanner
        scanner.close();
    }
}

