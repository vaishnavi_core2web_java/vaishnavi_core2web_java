/*4. Write a program to print the sum of odd elements in an array.Take input from the user.
Example:
Enter size: 10
Array:
1 2 3 4 2 5 6 2 8 10
Output :
Sum of odd elements : 9 */
import java.util.Scanner;
class p4 {
    public static void main(String[] args) {
        // Define the size of the array
        int size = 10;
        int[] array = new int[size];

        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the elements of the array
        System.out.println("Please enter 10 elements for the array:");

        // Read the elements from the user
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        // Initialize a variable to store the sum of odd elements
        int sumOfOddElements = 0;

        // Iterate through the array and sum the odd elements
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                sumOfOddElements += array[i];
            }
        }

        // Print the sum of odd elements
        System.out.println("Sum of odd elements: " + sumOfOddElements);

        // Close the scanner
        scanner.close();
    }
}

