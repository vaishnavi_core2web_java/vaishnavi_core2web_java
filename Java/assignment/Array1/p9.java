/*Write a program where you have to print the odd indexed elements. Take input from
the user
Example :
Enter size: 10.
1 2 3 4 5 6 7 8 9 10
2 is an odd indexed element
4 is an odd indexed element
6 is an odd indexed element
8 is an odd indexed element
10 is an odd indexed element*/
import java.util.Scanner;

 class p9 {
    public static void main(String[] args) {
        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the size of the array
        System.out.print("Enter size: ");
        int size = scanner.nextInt();

        // Create an array to store the elements
        int[] array = new int[size];

        // Prompt the user to enter the elements of the array
        System.out.println("Please enter " + size + " elements:");

        // Read the elements from the user
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }

        // Print the elements at odd indices
        System.out.println("Odd indexed elements:");
        for (int i = 1; i < size; i += 2) {
            System.out.println(array[i] + " is an odd indexed element");
        }

        // Close the scanner
        scanner.close();
    }
}

