/*Write a program where you have to take input from the user for a character array and
print the characters.*/
import java.util.Scanner;

 class p6
{
    public static void main(String[] args) {
        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the size of the character array
        System.out.print("Enter the size of the character array: ");
        int size = scanner.nextInt();

        // Create an array to store the characters
        char[] charArray = new char[size];

        // Prompt the user to enter the characters
        System.out.println("Please enter " + size + " characters:");

        // Read the characters from the user
        for (int i = 0; i < size; i++) {
            // Read the next character
            charArray[i] = scanner.next().charAt(0);
        }

        // Print the characters
        System.out.println("The characters you entered are:");
        for (char c : charArray) {
            System.out.print(c + " ");
        }

        // Close the scanner
        scanner.close();
    }
}

