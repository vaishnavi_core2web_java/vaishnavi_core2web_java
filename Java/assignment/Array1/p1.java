class p1 {
    public static void main(String[] args) {
       
        int[] array = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

       
        StringBuilder output = new StringBuilder();

        
        for (int i = 0; i < array.length; i++) {
            output.append(array[i]);
            if (i < array.length - 1) {
                output.append(", ");
            }
        }

       
        System.out.println(output.toString());
    }
}

