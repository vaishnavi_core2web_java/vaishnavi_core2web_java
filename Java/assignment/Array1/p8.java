/*8. Write a program where you have to store the employee’s age working at a company,
take count of employees from the user.Take input from the user.*/import java.util.Scanner;
class p8 {
    public static void main(String[] args) {
        // Create a Scanner object to take input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the count of employees
        System.out.print("Enter the count of employees: ");
        int employeeCount = scanner.nextInt();

        // Create an array to store the ages of employees
        int[] employeeAges = new int[employeeCount];

        // Prompt the user to enter the ages of employees
        System.out.println("Please enter the ages of " + employeeCount + " employees:");

        // Read the ages from the user
        for (int i = 0; i < employeeCount; i++) {
            System.out.print("Employee " + (i + 1) + " age: ");
            employeeAges[i] = scanner.nextInt();
        }

        // Print the ages of employees
        System.out.println("Ages of employees:");
        for (int i = 0; i < employeeCount; i++) {
            System.out.println("Employee " + (i + 1) + " age: " + employeeAges[i]);
        }

        // Close the scanner
        scanner.close();
    }
}

