/*4.WAP to search a specific element in an array and return its index. Ask the user to
provide the number to search, also take size and elements input from the user. */
import java.util.Scanner;

class SearchElement {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.print("Enter the element to search: ");
        int elementToSearch = scanner.nextInt();

        int index = -1; // Initialize index to -1 (not found)
        for (int i = 0; i < size; i++) {
            if (arr[i] == elementToSearch) {
                index = i;
                break; // Exit loop if element found
            }
        }

        if (index != -1) {
            System.out.println("Element found at index: " + index);
        } else {
            System.out.println("Element not found in the array");
        }

        scanner.close();
    }
}
