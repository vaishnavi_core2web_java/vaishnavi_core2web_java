/*9. WAP to print the minimum element in the array, where you have to take the size and
elements from the user. */
import java.util.Scanner;

class FindMinimumElement {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        int min = Integer.MAX_VALUE; // Initialize min with maximum integer value
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
            min = Math.min(min, arr[i]); // Update min if current element is smaller
        }
        System.out.println("Minimum element in the array: " + min);
        scanner.close();
    }
}
