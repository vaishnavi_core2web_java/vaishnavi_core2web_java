/*7.WAP to print the array , if the user given size of an array is even then print the alternate
elements in an array, else print the whole array. */
import java.util.Scanner;

class PrintArrayBasedOnSize {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.println("The entered array:");
        if (size % 2 == 0) { // Check if size is even
            for (int i = 0; i < size; i += 2) {
                System.out.print(arr[i] + " ");
            }
        } else {
            for (int num : arr) { // Print entire array if size is odd
                System.out.print(num + " ");
            }
        }
        scanner.close();
    }
}
