/*3.WAP to check if there is any vowel in the array of characters if present then print its
index, where you have to take the size and elements from the user. */
import java.util.Scanner;

class VowelsInCharArray {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the character array: ");
        int size = scanner.nextInt();
        char[] charArray = new char[size];
        System.out.println("Enter the characters of the array:");
        for (int i = 0; i < size; i++) {
            charArray[i] = scanner.next().charAt(0);
        }
        boolean vowelFound = false;
        System.out.println("Vowel indices:");
        for (int i = 0; i < size; i++) {
            char ch = charArray[i];
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
                vowelFound = true;
                System.out.print(i + " ");
            }
        }
        if (!vowelFound) {
            System.out.println("No vowels found");
        }
        scanner.close();
    }
}
