/*1.WAP to count the even numbers in an array where you have to take the size and
elements from the user. And also print the even numbers too */
import java.util.Scanner;

class CountEvenNumbers {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        int evenCount = 0;
        System.out.println("Even elements:");
        for (int num : arr) {
            if (num % 2 == 0) {
                evenCount++;
                System.out.print(num + " ");
            }
        }
        System.out.println("\nTotal even numbers: " + evenCount);
        scanner.close();
    }
}
