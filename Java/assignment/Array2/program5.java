/*5.Write a program to print the sum of odd indexed elements, in an array. Where you have
to take size input and elements input from the user . */
import java.util.Scanner;

class SumOfOddIndexed {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        int sum = 0;
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
            sum += (i % 2 != 0) ? arr[i] : 0; // Add element if i is odd (using ternary operator)
        }
        System.out.println("Sum of odd indexed elements: " + sum);
        scanner.close();
    }
}
