/*6.Write a program to print the products of odd indexed elements in an array. Where you
have to take size input and elements input from the user. */
import java.util.Scanner;

class ProductOfOddIndexed {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        int product = 1; // Initialize product with 1 to avoid multiplication by 0
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
            product *= (i % 2 != 0) ? arr[i] : 1; // Multiply product by element if i is odd
        }
        if (product == 1) {
            System.out.println("Product of odd indexed elements: No odd elements or all elements are 0");
        } else {
            System.out.println("Product of odd indexed elements: " + product);
        }
        scanner.close();
    }
}
