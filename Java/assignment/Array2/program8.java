/*8.WAP to print the elements in an array which are greater than 5 but less than 9, where
you have to take the size and elements from the user. */
import java.util.Scanner;

class ElementsBetweenFiveAndNine {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        System.out.println("Elements between 5 and 9 (exclusive):");
        for (int num : arr) {
            if (num > 5 && num < 9) {
                System.out.print(num + " ");
            }
        }
        scanner.close();
    }
}
