/*2.WAP to print the sum of elements divisible by 3 in the array, where you have to take the
size and elements from the user. */
import java.util.Scanner;

class SumDivisibleByThree {

    static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the size of the array: ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        int sum = 0;
        System.out.println("Elements divisible by 3:");
        for (int num : arr) {
            if (num % 3 == 0) {
                sum += num;
                System.out.print(num + " ");
            }
        }
        System.out.println("\nSum of elements divisible by 3: " + sum);
        scanner.close();
    }
}
