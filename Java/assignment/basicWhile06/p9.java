class p9 {
    public static void main(String[] args) {
        int sum = 0;
        int number = 150;
        
        while (number >= 101) {
            // Check if the number is odd
            if (number % 2 != 0) {
                sum += number;
            }
            number--;
        }
        
        System.out.println("The sum of odd numbers from 150 to 101 is: " + sum);
    }
}

