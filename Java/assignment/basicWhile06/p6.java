 class p6 {
    public static void main(String[] args) {
        char letter = 'a';
        
        while (letter <= 'z') {
            
            if (letter != 'a' && letter != 'e' && letter != 'i' && letter != 'o' && letter != 'u') {
                System.out.print(letter + " ");
            }
            letter++;
        }
    }
}

