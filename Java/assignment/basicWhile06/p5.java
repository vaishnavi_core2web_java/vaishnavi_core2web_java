 class p5 {
    public static void main(String[] args) {
        // Initialize the number of days until the assignment is due
        int daysUntilDue = 10; // You can set this to any starting number of days

        // While loop to print the countdown of days
        while (daysUntilDue >= 0) {
            if (daysUntilDue == 0) {
                System.out.println("Today is the due date! Submit your assignment.");
            } else {
                System.out.println("Days until assignment is due: " + daysUntilDue);
            }
            daysUntilDue--;
        }
    }
}

