 class p1
{
    public static void main(String[] args) {
        // Example usage
        printNumbersLeavingTwo(1, 10);
    }

    public static void printNumbersLeavingTwo(int start, int end) {
        for (int number = start; number <= end; number += 3) {
            System.out.println(number);
        }
    }
}

