 class p9 {

    public static void main(String[] args) {
        int number = 214367689;
        int oddCount = 0;
        int evenCount = 0;

        while (number > 0) {
            int digit = number % 10;  // Get the last digit
            if (digit % 2 == 0) {
                evenCount++;  // Increment even count if the digit is even
            } else {
                oddCount++;   // Increment odd count if the digit is odd
            }
            number /= 10;     // Remove the last digit
        }

        System.out.println("Odd count : " + oddCount);
        System.out.println("Even count : " + evenCount);
    }
}
