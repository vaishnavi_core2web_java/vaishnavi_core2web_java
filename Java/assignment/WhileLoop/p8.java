class p8{
	public static void main(String[] args){
		int number = 216985;
		printodddigit(number);
	}
	public static void printodddigits(int number){
		StringBuilder odddigitReversed = new StringBuilder();
		while(number>0){
			int digit = number %10;
			if(digit %2!=0){
				odddigitReversed.append(digit).append(" ");
			}
			number/= 10;
		}
	System.out.println(odddigitReversed.reverse().toString().trim());
	}
}

