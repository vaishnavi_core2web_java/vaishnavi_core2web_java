 class p10 {

    public static void main(String[] args) {
        long number = 9307922405L;  // Use long for large numbers
        long tempNumber = number;   // Temporary variable to avoid modifying the original number
        int sum = 0;

        // Loop through each digit in the number
        while (tempNumber > 0) {
            int digit = (int) (tempNumber % 10);  // Get the last digit
            sum += digit;                        // Add the digit to the sum
            tempNumber /= 10;                    // Remove the last digit
        }

        // Print the result
        System.out.println("Sum of digits in " + number + " is " + sum);
    }
}
