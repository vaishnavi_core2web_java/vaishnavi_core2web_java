import java.util.Scanner;
class p9 {

    public static void main(String[] args) {
        final int PASSING_MARK = 40;
        Scanner scanner = new Scanner(System.in);

        int[] marks = new int[5];
        String[] subjects = {"Math", "Science", "English", "History", "Geography"};

        System.out.println("Enter marks for the following subjects (out of 100):");

        for (int i = 0; i < subjects.length; i++) {
            System.out.print(subjects[i] + ": ");
            marks[i] = scanner.nextInt();
        }

        if (isPassed(marks, PASSING_MARK)) {
            int totalMarks = calculateTotal(marks);
            String grade = getGrade(totalMarks);
            System.out.println("Your grade is: " + grade);
        } else {
            System.out.println("You failed the exam");
        }

        scanner.close();
    }

    public static boolean isPassed(int[] marks, int passingMark) {
        for (int mark : marks) {
            if (mark < passingMark) {
                return false;
            }
        }
        return true;
    }

    public static int calculateTotal(int[] marks) {
        int total = 0;
        for (int mark : marks) {
            total += mark;
        }
        return total;
    }

    public static String getGrade(int totalMarks) {
        if (totalMarks >= 450) {
            return "First class with distinction";
        } else if (totalMarks >= 300) {
            return "First class";
        } else if (totalMarks >= 200) {
            return "Second class";
        } else {
            return "Fail";
        }
    }
}
