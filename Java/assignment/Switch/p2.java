import java.util.Scanner;
class p2 {
    public static void main(String[] args) {
        // Create a Scanner object to read input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter a grade
        System.out.print("Enter your grade (A, B, C, D, or F): ");
        char grade = scanner.next().charAt(0);

        // Close the scanner to prevent resource leak
        scanner.close();

        // Print remark according to grade
        switch (grade) {
            case 'A':
            case 'a':
                System.out.println("Excellent! Keep up the good work!");
                break;
            case 'B':
            case 'b':
                System.out.println("Very good! You are doing well.");
                break;
            case 'C':
            case 'c':
                System.out.println("Good! You are on the right track.");
                break;
            case 'D':
            case 'd':
                System.out.println("Satisfactory. You can improve.");
                break;
            case 'F':
            case 'f':
                System.out.println("Needs improvement. Keep working hard.");
                break;
            default:
                System.out.println("Invalid grade entered. Grade should be A, B, C, D, or F.");
                break;
        }
    }
}

