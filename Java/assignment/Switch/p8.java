import java.util.Scanner;
class p8 {
    public static void main(String[] args) {
        // Create a Scanner object to read input from the user
        Scanner scanner = new Scanner(System.in);

        // Prompt the user to enter the first number
        System.out.print("Enter the first number: ");
        int num1 = scanner.nextInt();

        // Prompt the user to enter the second number
        System.out.print("Enter the second number: ");
        int num2 = scanner.nextInt();

        // Close the scanner to prevent resource leak
        scanner.close();

        // Check if both numbers are positive
        if (num1 > 0 && num2 > 0) {
            // Multiply the numbers
            int result = num1 * num2;

            // Check if the result is even or odd using a switch statement
            switch (result % 2) {
                case 0:
                    System.out.println("Result is even. Sorry, terminating the program.");
                    break;
                case 1:
                    System.out.println("Result is odd.");
                    break;
                default:
                    System.out.println("Invalid result.");
                    break;
            }
        } else {
            System.out.println("Both numbers should be positive.");
        }
    }
}

