import java.util.*;

class p3{
        static int factors(int num){
                int count = 0;
                for(int i =1;i<=Math.sqrt(num);i++){
                        if (num%i==0)
                                if (i == num/i)
                                   count = count + 1;
                                else
                                   count = count + 2;

                }
                return count;
        }
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                int num = sc.nextInt();
                int count = factors(num);
                System.out.println(count);
        }
}

/*import java.util.*;

class p3 {
    static int factors(int num) {
        int count = 0;
        for (int i = 1; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                // If i divides num evenly, increment count
                count++;
                // If i is not the square root of num, then its pair is also a factor
                if (i != Math.sqrt(num)) {
                    count++;
                }
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int num = sc.nextInt();
        int count = factors(num);
        System.out.println("Number of factors: " + count);
    }
}*/

