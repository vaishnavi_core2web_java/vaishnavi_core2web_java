// in this code we written num as num / 2 as we know that the factors are only find before the num/2 valer ex: 10/2 =5 factors of 10 are 1 2 5 thus only the number itself remains


import java.util.*;

class findfactor{
        static int factors(int num){
                int count = 0;
                int itr = 0;
                for(int i =1;i<=num/2;i++){
                        itr ++;
                        if (num%i==0)
                                count = count + 1;
                }
                System.out.println("iteration"+itr);
                return count+1;
        }
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                int num = sc.nextInt();
                int count = factors(num);
                System.out.println(count);
        }
}
