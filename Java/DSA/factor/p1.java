// this code is the basic code of factor in which the total count of factor is given for ex: 10 factoral is 1,2,5,10 thus the count is 4
import java.util.*;

class findfactor{
        static int factors(int num){
                int count = 0;
                for(int i =1;i<=num;i++){
                        if (num%i==0)
                                count ++;
                }
                return count;
        }
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                int num = sc.nextInt();
                int count = factors(num);
                System.out.println(count);
        }
}

//but this code do not satisfies the optimisation of the code
